export interface Task {
  done: boolean;
  description: string;
}
